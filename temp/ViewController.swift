//
//  ViewController.swift
//  temp
//
//  Created by Goutham Kumar on 15/05/20.
//  Copyright © 2020 Goutham Kumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, URLSessionDelegate {
    var session: URLSession!
    lazy var downloadsSession: URLSession = {
      let configuration = URLSessionConfiguration.background(withIdentifier:
        "bgDownloader")
//        configuration.sessionSendsLaunchEvents = true
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
      return session
    } ()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("started")
       
        self.session = self.downloadsSession
        print("done")
        // Do any additional setup after loading the view.
    }
}

